<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use App\User;
use Validator;

class TokensController extends Controller
{
   

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $validator = Validator::make($credentials, [
            'email' => 'required|email',
            'password'=>'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'success'=>false,
                'message'=>'Error de validación',
                'errors'=>$validator->errors()
            ],422);
        }
        //Todo fue perfecto con las credenciales
        $token= JWTAuth::attempt($credentials);

        if($token){
            return response()->json([
                'success'=>true,
                'token'=>$token,
                'user'=>User::where('email', $credentials['email'])->get()->first()
            ],200);
        }else{
            return response()->json([
                'success'=>false,
                'message'=>'Error de credenciales',
                'errors'=>$validator->errors()
            ],401);
        }
       
    }

    public function refreshToken(){
        $token= JWTAuth::getToken();
        try{
            $token = JWTAuth::refresh($token);
            return response()->json([
                'success'=>true,
                'token'=>$token],200);
        }catch (TokenExpiredException $ex){

            return response()->json([
                'success'=>false,
                'message'=>'Necesitas realizar el login de nuevo porque tu token ha expirado'
            ],422);

        }catch (TokenBlacklistedException $ex){

            return response()->json([
                'success'=>false,
                'message'=>'Necesitas realizar el login de nuevo porque no se pudo actualizar el token'   
            ],422);
        }
    }

    public function logout(Request $request)
    {
        $token= JWTAuth::getToken();

        try{
            JWTAuth::invalidate($token);
            return response()->json([
                'success'=>true,
                'message'=>'Cerraste sesión de manera correcta'],200);
        }catch (JWTExeption $ex){

            return response()->json([
                'success'=>false,
                'message'=>'Fallo al cerrar sesión '],422);

        }
       
    }
}
