<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Users
        Permission::create([
            'name' =>'Navegar usuarios',
            'slug' =>'users.index',
            'description'=>'Listar y navega todos los usuarios del sistema',
        ]);
        Permission::create([
            'name' =>'Ver detalle de usuario',
            'slug' =>'users.show',
            'description'=>'Ver en detalle cada usuario del sistema',
        ]);
        Permission::create([
            'name' =>'Edicion de usuarios',
            'slug' =>'users.edit',
            'description'=>'Editar cualquier usuario del sistema',
        ]);
        Permission::create([
            'name' =>'Eliminar usuarios',
            'slug' =>'users.destroy',
            'description'=>'Eliminar cualquier usuario  del sistema',
        ]);

        //Roles
        Permission::create([
            'name' =>'Navegar roles',
            'slug' =>'roles.index',
            'description'=>'Listar y navega todos los roles del sistema',
        ]);
        Permission::create([
            'name' =>'Ver detalle de rol',
            'slug' =>'roles.show',
            'description'=>'Ver en detalle cada rol del sistema',
        ]);
        Permission::create([
            'name' =>'Creación de roles',
            'slug' =>'roles.create',
            'description'=>'Crear roles en el sistema',
        ]);
        Permission::create([
            'name' =>'Edicion de roles',
            'slug' =>'roles.edit',
            'description'=>'Editar cualquier rol del sistema',
        ]);
        Permission::create([
            'name' =>'Eliminar rol',
            'slug' =>'roles.destroy',
            'description'=>'Eliminar cualquier rol  del sistema',
        ]);

        //Clientes
        Permission::create([
            'name' =>'Navegar clientes',
            'slug' =>'clientes.index',
            'description'=>'Listar y navega todos los clientes del sistema',
        ]);
        Permission::create([
            'name' =>'Ver detalle de cliente',
            'slug' =>'clientes.show',
            'description'=>'Ver en detalle cada cliente del sistema',
        ]);
        Permission::create([
            'name' =>'Creación de clientes',
            'slug' =>'clientes.create',
            'description'=>'Crear clientes en el sistema',
        ]);
        Permission::create([
            'name' =>'Edicion de cliente',
            'slug' =>'clientes.edit',
            'description'=>'Editar cualquier cliente del sistema',
        ]);
        Permission::create([
            'name' =>'Eliminar cliente',
            'slug' =>'clientes.destroy',
            'description'=>'Eliminar cualquier cliente  del sistema',
        ]);
    }
}
