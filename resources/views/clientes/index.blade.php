@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                <h2> Clientes</h2>
                 @can('clientes.create')
                 <a href="{{ route('clientes.create') }}"
                 class="btn btn-sm btn-primary pull-right">
                 Crear
                 </a>
                 @endcan
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="10px">ID</th>
                                <th>Nombre</th>
                                <th colspan="3"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($clientes as $cliente)
                            <tr>
                                <td>{{ $cliente->id }}</td>
                                <td>{{ $cliente->name }}</td>
                                <td>
                                    @can('clientes.show')
                                        <a href="{{ route('clientes.show',$cliente->id) }}"
                                        class="btn btn-sm btn-default">
                                        Ver
                                        </a>
                                    @endcan
                                
                                </td>
                                <td>
                                    @can('clientes.edit')
                                        <a href="{{ route('clientes.edit',$cliente->id) }}"
                                        class="btn btn-sm btn-default">
                                        Editar
                                        </a>
                                    @endcan
                                
                                </td>
                                <td>
                                    @can('clientes.destroy')
                                    {!! Form::open(['route'=>['clientes.destroy',$cliente->id],
                                    'method' => 'POST']) !!}
                                       <button class="btn btn-sm btn-danger">
                                       Eliminar
                                       </button>
                                    {!! Form::close() !!}
                                    @endcan
                                
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection