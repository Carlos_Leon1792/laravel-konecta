<div class="form-group">
    {{Form::label('name','Nombre del cliente')}}
    {{Form::text('name',null,['class'=>'form-control'])}}
</div>
<div class="form-group">
    {{Form::label('adress','Dirección del cliente')}}
    {{Form::text('adress',null,['class'=>'form-control'])}}
</div>

<div class="form-group">
    {{Form::submit('Guardar',null,['class'=>'btn btn-sm btn.primary'])}}
</div>