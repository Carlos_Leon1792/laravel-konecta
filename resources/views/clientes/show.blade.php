@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                <h2> Clientes</h2>
                </div>
                <div class="panel-body">
                 <table>
                    <tr>
                        <td>Nombre:</td>
                        <td>{{ $clientes->name }}</td>
                    </tr> 
                    <tr>
                        <td>Dirección:</td>
                        <td>{{ $clientes->email }}</td>
                    </tr> 
                </table>
                 </div>
            </div>
        </div>
    </div>
</div>
@endsection