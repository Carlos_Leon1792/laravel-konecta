@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                <h2> Usuario</h2>
                </div>
                <div class="panel-body">
                 {!! Form::model($user, ['route' => ['users.update', $user->id],
                 'method'=>'PATCH']) !!}

                    @include('users.partials.form')

                {!! Form::close() !!}
                 </div>
            </div>
        </div>
    </div>
</div>
@endsection