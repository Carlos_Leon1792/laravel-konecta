@extends('layouts.app')

@section('content')
<div   class="container">
    <div id="app" class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                 <h2>Usuarios</h2>

                </div>

                <div class="col-md-12">
                    <div class="page-header">
                    
                        <h3>Busquedad de usuarios</h3>
                        <div class="form-group">
                        <input type="text" class="form-control"  v-model="busquedad_nombre" maxlength="128" required>
        
                        </div>
                        <div class="form-group">
                        
                        <button class="btn btn-success" @click="onFind()" data-dismiss="modal" 
                             style="background: #02b3e4;">Buscar</button>
                        </div>  
                    </div>


                </div>

                <div class="panel-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="10px">ID</th>
                                <th>Nombre</th>
                                <th colspan="3"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td width="10px">
                                    @can('users.show')
                                        <a href="{{ route('users.show',$user->id) }}"
                                        class="btn btn-sm btn-default">
                                        Ver
                                        </a>
                                    @endcan
                                
                                </td>
                                <td width="10px">
                                    @can('users.edit')
                                        <a href="{{ route('users.edit',$user->id) }}"
                                        class="btn btn-sm btn-default">
                                        Editar
                                        </a>
                                    @endcan
                                
                                </td>
                                <td width="10px">
                                    @can('users.destroy')
                                    {!! Form::open(['route'=>['users.destroy',$user->id],
                                    'method' => 'POST']) !!}
                                       <button class="btn btn-sm btn-danger">
                                       Eliminar
                                       </button>
                                    {!! Form::close() !!}
                                    @endcan
                                
                                </td>
                            </tr>
                            @endforeach
   
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
  
</div>

@endsection