<div class="form-group">
    {{Form::label('name','Nombre del usuario')}}
    {{Form::text('name',null,['class'=>'form-control'])}}
</div>
<hr>

<hr>Lista de Roles</h3>
<div class="form-group">
    <ul>
        @foreach($roles as $role)
        <li>
            <label>
                {{ Form::checkbox('roles[]', $role->id ,null )}}
                {{ $role->name}}
                <em>({{ $role->description  ?: 'Sin descripción'}})</em>
            </label>
        </li>
        @endforeach
    </ul>
<div class="form-group">
    {{Form::submit('Guardar',null,['class'=>'btn btn-sm btn.primary'])}}
</div>