<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

    //Routes 

    Route::middleware(['auth'])->group(function(){
        
      //Clientes
       
      
        Route::post('clientes/store', 'ClientesController@store')->name('clientes.store');

        Route::get('clientes', 'ClientesController@index')->name('clientes.index');

        Route::get('clientes/create', 'ClientesController@create')->name('clientes.create');

        Route::post('/update/{clientes}', 'ClientesController@update')->name('clientes.update');

        Route::get('clientes/{clientes}', 'ClientesController@show')->name('clientes.show');

        Route::post('clientes/{clientes}', 'ClientesController@destroy')->name('clientes.destroy');

        Route::get('clientes/{clientes}/edit', 'ClientesController@edit')->name('clientes.edit');



        //Users
       
        Route::get('users', 'UserController@index')->name('users.index');

        Route::patch('/users/{user}/update/',  'UserController@update')->name('users.update');

        Route::get('users/{user}', 'UserController@show')->name('users.show');

        Route::post('users/{user}', 'UserController@destroy')->name('users.destroy');

        Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit');

});
